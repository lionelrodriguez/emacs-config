#!/bin/bash

dateTimestamp=`echo $(date +%Y%m%d_%H%M%S)`

emacsBackupFile=~/emacs.backup-${dateTimestamp}.tar.gz
emacsConfigFile=~/.emacs
emacsDirectory=~/.emacs.d
urlScript="https://gitlab.com/lionelrodriguez/emacs-config/raw/master/.emacs"

function checkErrCode(){
 if [ $1 -ne 0 ]; then
   echo "Ocurrió un error."
   exit 1
 fi
}

backupConfig(){
  if [ -f ${emacsConfigFile} ]; then
    echo "Existe archivo de configuracion".
    if [ -d ${emacsDirectory} ]; then
      echo "Existe directorio ${emacsDirectory}"
      tar cvf ${emacsBackupFile} ${emacsDirectory}
      checkErrCode $?
      tar uvf ${emacsBackupFile} ${emacsConfigFile}
      checkErrCode $?
      rm -rvf ${emacsDirectory}
      checkErrCode $?
      mkdir -p ${emacsDirectory}
      checkErrCode $?
    fi

    mv -v ${emacsConfigFile} ${emacsConfigFile}.original
    checkErrCode $?
  fi
}

getEmacsConfigFile(){
  curl -O ${urlScript}
  checkErrCode $?
}

errCode=1
backupConfig
getEmacsConfigFile