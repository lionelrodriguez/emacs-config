# emacs-config

This configuration file is basically the same of [Arjen Wiersma](https://gitlab.com/credmp), i recommend strongly check her projects.  

  - [Arjen emacs configuration repository](https://gitlab.com/buildfunthings/emacs-config)
 
You need the fonts mentioned above, which are both used by the configuration:

 - [Hack](https://sourcefoundry.org/hack/)
 - Inconsolata (Provided by Arch Linux en community packages)
 